
export const state = () => ({
  menuItems: [],
  stickies: [],
  rankings: [],
  postsByCategory: [],
  categoryName: '',
  postsInCategory: [],
  showMorePosts: true,
  singlePost: {}
})

export const getters = {
  getCategoryNameBySlug (state) {
    return (slug) => {
      for (let i = 0; i < state.menuItems.length; i++) {
        if (state.menuItems[i].to.includes(slug)) {
          return state.menuItems[i].title
        }
      }
    }
  }
}

export const mutations = {
  setMenu (state, items) {
    state.menuItems = items
  },
  setStickies (state, posts) {
    state.stickies = posts
  },
  setRankings (state, posts) {
    state.rankings = posts
  },
  setPostsByCategory (state, posts) {
    state.postsByCategory = posts
  },
  setPostsInCategory (state, posts) {
    state.postsInCategory = posts
  },
  setPostsInCategoryPaged (state, posts) {
    state.postsInCategory = [...state.postsInCategory, ...posts]
  },
  setShowMorePosts (state, value) {
    state.showMorePosts = value
  },
  setSinglePost (state, value) {
    state.singlePost = value
  }
}

export const actions = {
  async fetchMenu (context) {
    console.log('fetchMenu')
    await this.$axios.$get('/wp/v2/categories')
      .then((data) => {
        const aux = data
        const menu = []
        menu.push({
          title: 'Home',
          to: '/'
        })
        for (let index = 0; index < aux.length; index++) {
          menu.push({
            id: aux[index].id,
            title: aux[index].name,
            to: '/category/' + aux[index].slug
          })
        }
        context.commit('setMenu', menu)
      })
      .catch((error) => {
        console.log('notify the error', error)
        const menu = [
          {
            title: 'Home',
            to: '/'
          },
          {
            id: 4,
            title: 'Culture',
            to: '/category/culture'
          },
          {
            id: 3,
            title: 'Economy',
            to: '/category/economy'
          },
          {
            id: 5,
            title: 'Farandula',
            to: '/category/farandula'
          },
          {
            id: 6,
            title: 'Police',
            to: '/category/police'
          },
          {
            id: 1,
            title: 'Politics',
            to: '/category/politics'
          },
          {
            id: 7,
            title: 'Science',
            to: '/category/science'
          },
          {
            id: 2,
            title: 'Sports',
            to: '/category/sports'
          }
        ]
        context.commit('setMenu', menu)
      })
  },
  async fetchHomePosts (context) {
    await this.$axios.$get('/snapi/v1/home')
      .then((data) => {
        context.commit('setStickies', data.stickies)
        context.commit('setRankings', data.rankings)
        context.commit('setPostsByCategory', data.categories)
      })
      .catch(({ response }) => {
        console.log('notify the error', response)
      })
  },
  async fetchPostsForCategoryPage (context, { slug, page }) {
    console.log('slug=', slug, 'page=', page)
    await this.$axios.$get('/snapi/v1/sncategory/' + slug + '/' + page)
      .then((data) => {
        if (page === 1) {
          context.commit('setPostsInCategory', data)
        } else {
          context.commit('setPostsInCategoryPaged', data)
        }
      })
      .catch(({ response }) => {
        console.log('notify the error', response)
      })
    await this.$axios.$get('/snapi/v1/sncategory/' + slug + '/' + (page + 1))
      .then((data) => {
        if (data.length === 0) {
          context.commit('setShowMorePosts', false)
        } else {
          context.commit('setShowMorePosts', true)
        }
      })
      .catch(({ response }) => {
        console.log('notify the error', response)
      })
  },
  async fetchSinglePost (context, { slug }) {
    console.log('slugPost = ', slug)
    await this.$axios.$get('/snapi/v1/snsingle/' + slug)
      .then((data) => {
        if (data.length) {
          context.commit('setSinglePost', data[0])
        } else {
          context.commit('setSinglePost', null)
        }
      })
      .catch(({ response }) => {
        console.log('notify the error', response)
      })
  },
  async fetchRankings (context) {
    await this.$axios.$get('/snapi/v1/snrankings')
      .then((data) => {
        context.commit('setRankings', data)
      })
      .catch(({ response }) => {
        console.log('notify the error', response)
      })
  }
}
